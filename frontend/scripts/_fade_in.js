import mezr from 'mezr/mezr'


class FadeIn {

    elements = [];
    scrollPosition = 0;
    windowHeight = 0;
    callback = null;

    constructor(selector, callback=null){
        this.callback = callback;
        this.windowHeight = window.innerHeight;

        for(let el of document.querySelectorAll(selector)){
            this.elements.push({
                el,
                top: mezr.offset(el).top,
                bottom: mezr.offset(el).top + mezr.height(el),
                getRealBottom: () => {
                    let elHeight = mezr.height(el),
                        threshold = this.windowHeight - 100;
                    return mezr.offset(el).top + (elHeight > threshold ? threshold : elHeight)
                },
                active: false
            })
        }

        window.addEventListener('scroll', this._handlerScroll.bind(this));
        window.addEventListener('resize', () => this.windowHeight = window.innerHeight);
        this._handlerScroll();
    }

    _handlerScroll(){
        this.scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
        this.markVisibleAsActive();
    }

    get inactiveElements(){
        return this.elements.filter(el => !el.active);
    }

    markVisibleAsActive(){
        for(let element of this.inactiveElements){
            if(element.top >= this.scrollPosition && element.getRealBottom() <= this.scrollPosition+this.windowHeight) {
                element.el.classList.add("active");
                if(typeof this.callback === 'function') this.callback();
            }
        }
    }

}

export default FadeIn
