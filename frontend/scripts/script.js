import './_parallax'
import FadeIn from './_fade_in'
import price from './_price'
import Calculator from './_calculator'


new FadeIn(".fade-in, .stepList, .itemFly");
new FadeIn(".order", () => {
    setTimeout(() => {
        price.update(42000);
    }, 800);
});

new Calculator();
