import Odometer from './3p/odometer'

const priceElement = document.getElementById("price");

const price = new Odometer({
    el: priceElement,
    format: '( ddd)',
    theme: 'minimal',
    value: priceElement.innerHTML,
    duration: 8000
});


export default price
