class Calculator {
    elPrice = document.getElementById("calcPrice");
    elQuantity = document.getElementById("calcQuantity");
    elTime = document.getElementById("calcTime");

    price = 500.0;
    quantity = 100.0;
    tablePrice = 42000.0;

    constructor(){
        this.elPrice.addEventListener('change', this._handlerChangePrice.bind(this));
        this.elPrice.addEventListener('keyup', this._handlerChangePrice.bind(this));
        this.elQuantity.addEventListener('change', this._handlerChangeQuantity.bind(this));
        this.elQuantity.addEventListener('keyup', this._handlerChangeQuantity.bind(this));
        this.calculate();
    }

    setPrice(n){
        this.price = this.getNumber(n);
        this.elPrice.value = this.formatNumber(this.price);
        this.calculate();
    }
    setQuantity(n){
        this.quantity = this.getNumber(n);
        this.elQuantity.value = this.formatNumber(this.quantity);
        this.calculate();
    }
    setTime(n, ...words){
        this.elTime.value = `${n} ${this.word4number(n, ...words)}`;
    }

    getNumber(n){
        let num = parseInt(n.replace(" ", ""));
        if(isNaN(num)) num = 0;
        return num;
    }

    formatNumber(n){
        return(`${n}`.replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 '));
    }

    word4number(number, word_one, word_two, word_many) {
        number = parseInt(number);
        if(isNaN(number)) {
            return '';
        }
        if(10 < number && number < 20) {
            return word_many;
        }
        let lastDigit = number % 10;
        if (lastDigit == 1) {
            return word_one;
        } else if(2 <= lastDigit && lastDigit <= 4) {
            return word_two;
        } else {
            return word_many;
        }
    }


    _handlerChangePrice(e){
        this.setPrice(e.target.value);
    }

    _handlerChangeQuantity(e){
        this.setQuantity(e.target.value);
    }

    calculate(){
        let i = this.price * this.quantity,
            m = this.tablePrice / ( i * .4 ),
            t;

        if (m >= 12) {
            t = Math.ceil(m / 12);
            this.setTime(t, "год", "года", "лет")
        } else if (m <= 1) {
            t = Math.ceil(m * 30);
            this.setTime(t, "день", "дня", "дней")
        } else {
            t = Math.ceil(m);
            this.setTime(t, "месяц", "месяца", "месяцев")
        }
    }
}


export default Calculator
