<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
        <div class="footer">
            <div class="footer__copyright">© 3D Photolab</div>
            <div class="footer__owners">
                <a href="https://zh-ar.ru/" target="_blank"><img src="<?=SITE_TEMPLATE_PATH?>/images/footer__owners.png" class="footer__owners_logo"></a>
            </div>
        </div>
		<script src="<?=SITE_TEMPLATE_PATH?>/scripts/scripts.js"></script>
	</body>
</html>
