<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="equipment">
    <h3 class="equipment__header">Оборудование для переносной фотостудии</h3>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="equipment__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	<div class="equipment__item_wrap">
        <h4  class="equipment__item_name"><?= $arItem["~NAME"]?></h4>

        <div class="equipment__item_img"><img
                src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                class="equipment__item_img_value"
            ></div>
        <div class="equipment__item_price">
            <span class="equipment__item_price_value"><?= $arItem["DISPLAY_PROPERTIES"]["PRICE"]["DISPLAY_VALUE"]?> Р</span>
            <span class="equipment__item_price_quantity">(<?= $arItem["DISPLAY_PROPERTIES"]["QUANTITY"]["DISPLAY_VALUE"]?> шт.)</span>
        </div>
        <div class="equipment__item_description"><?= $arItem["PREVIEW_TEXT"];?></div>
        <a class="equipment__item_characteristics" href="#">Все характеристики</a>
        <button class="equipment__item_button">Добавить к заказу</button>
	</div>
	</div>
<?endforeach;?>
</div>


